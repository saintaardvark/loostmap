# -*- coding: utf-8 -*-

import logging
import os

import pandas as pd

from .utils import *

DIRNAME = os.path.dirname(__file__)
EU_CSV_FILE = 'data/interim/ve7cc_eu.csv'
DX_CSV_FILE = 'data/interim/ve7cc_dx.csv'
SUNSPOT_FILE = 'data/interim/SN_d_tot_V2.0-interim.csv'
DEFAULT_SKIMMER_FILE = '{}/../../{}'.format(DIRNAME, EU_CSV_FILE)
DEFAULT_SUNSPOT_FILE = '{}/../../{}'.format(DIRNAME, SUNSPOT_FILE)


def load_dataset(input_filepath, start_date=None, end_date=None):
    """Loads data into Pandas frame

    :param input_filepath: CSV file with skimmer data

    :param start_date: if provided, return data from no earlier than
    this date.

    :param end_date: if provided, return data from no later than this
    date
    """
    logger = logging.getLogger(__name__)
    logger.debug('loading data from file ' + input_filepath)
    data = pd.read_csv(input_filepath,
                       skipfooter=1,
                       engine='python',
                       keep_default_na=False,
                       index_col='date',
                       dtype={
                           'de_pfx': 'category',
                           'de_cont': 'category',
                           'band': 'category',
                           'dx_pfx': 'category',
                           'dx_cont': 'category',
                           'mode': 'category',
                           'tx_mode': 'category'
                       },
                       parse_dates=True)
    logger.debug('done!')
    data.drop(data.index[-1], inplace=True)
    if start_date is not None:
        data = data.loc[start_date:]
    if end_date is not None:
        data = data.loc[:end_date]
    return data


def load_sunspot_dataset(input_filepath):
    """
    Loads sunspot data into Pandas frame
    """
    logger = logging.getLogger(__name__)
    logger.debug('loading sunspot data from file ' + input_filepath)
    # I'm going to rename "SNvalue" to "sunspots_count", because I
    # keep reading "SN" as "supernova".
    sunspots = pd.read_csv(input_filepath,
                           header=None,
                           sep=";",
                           parse_dates=True,
                           names=["sunspots_count"])
    return sunspots


# TODO: Feels like there are too many data frames here.
# Try to simplify this more.
def load_and_count_by_band(skimmer_input_filepath=DEFAULT_SKIMMER_FILE,
                           sunspots_input_filepath=DEFAULT_SUNSPOT_FILE,
                           band='20m',
                           start_date='2019-01-01',
                           end_date='2019-03-31'):
    """Load skimmer data + sunspot data into frame, then return a subset
    filtered by band.

    Args:

    - skimmer_input_filepath: path to skimmer CSV file

    - sunspots_input_filepath: path to sunspot CSV file

    - band: which band to select

    - start_date: start date (inclusive) for data to return (default:
      2019-01-01)

    - end_date: end date (inclusive) for data to return (default:
      2019-03-31).

    """
    data = load_dataset(skimmer_input_filepath, start_date, end_date)
    data['total'] = 1
    hourly = count_by_hour(data, band)

    sunspots = load_sunspot_dataset(sunspots_input_filepath)
    hourly = add_sunspots_data(hourly, sunspots)

    add_time_columns(hourly)
    return hourly
