#!/usr/bin/env python3

import re
import shutil
import zipfile
from datetime import datetime, timedelta
from os import path
from pathlib import Path
from random import randint
from time import sleep

import click
import requests
from bs4 import BeautifulSoup
from dotenv import find_dotenv, load_dotenv

rbn_url = 'http://www.reversebeacon.net/raw_data/'
dl_url = 'http://www.reversebeacon.net/raw_data/dl.php'

project_dir = Path(__file__).resolve().parents[2]
# find .env automagically by walking up directories until it's found, then
# load up the .env entries as environment variables
load_dotenv(find_dotenv())

# TODO: Use pathlib for this
data_dir = f'{project_dir}/data/raw'
extract_dir = f'{project_dir}/data/external'


@click.group()
def rbn():
    """
    A tool to list and download datasets from the Reverse Beacon Network.
    """
    pass


@click.command('download', short_help='Download data set(s)')
# TODO: Change `default` to `required=True`
# TODO: Maybe make the default the latest? or today's/yesterday's date?
@click.option('--date',
              '-d',
              default=20190217,
              help='Date to download (format: YYYYMMDD).')
@click.option('--range',
              '-r',
              help="""
              Range to download (format: YYYYMMDD-YYYYMMDD).  Will
              sleep for 5-15 seconds between each download.
              """)
@click.option('--unzip/--no-unzip',
              default=True,
              show_default=True,
              help='Unzip file after downloading.')
@click.option('--dryrun/--no-dryun',
              default=False,
              help='Just show what would be done.')
@click.option('--download-dir',
              default=data_dir,
              type=click.Path(exists=True),
              show_default=True,
              help='Where to download to.')
@click.option('--skip-existing/--no-skip-existing',
              default=False,
              show_default=True,
              help='Skip re-downloading any existing files.')
@click.option('--extract-dir',
              default=extract_dir,
              type=click.Path(exists=True),
              show_default=True,
              help='Where to extract to. Implies --unzip.')
def download(date,
             range=None,
             unzip=False,
             dryrun=False,
             skip_existing=False,
             download_dir=data_dir,
             extract_dir=extract_dir):
    """
    Download one or more datasets
    """
    if range is None:
        download_day(date, unzip, dryrun, download_dir, extract_dir)
    else:
        start, end = split_range_arg(range)
        if not start:
            print('That range is bogus! Use "--help" option for details.')
            return
        print(start, end)
        step = timedelta(days=1)
        while start <= end:
            try:
                download_day(date=datetime.strftime(start, "%Y%m%d"),
                             unzip=unzip,
                             dryrun=dryrun,
                             skip_existing=skip_existing,
                             download_dir=download_dir,
                             extract_dir=extract_dir)
                print("Sleeping a bit between requests...")
                sleep(randint(5, 15))
            except RBNDataAlreadyDownloaded:
                print("Already downloaded, moving to the next one")
            start += step


def split_range_arg(range):
    """Check if range looks like it is two YYYYMMDD dates separated by
    hyphens, and return the two components if so.
    """
    r = re.compile(r'\d{8}\-\d{8}')
    if r.fullmatch(range):
        start, end = re.split(r'\-', range)
        start = datetime.strptime(start, '%Y%m%d')
        end = datetime.strptime(end, '%Y%m%d')
        return [start, end]
    return [None, None]


class RBNDataAlreadyDownloaded(Exception):
    """Raised when data was already downloaded"""


def download_day(date,
                 unzip=False,
                 dryrun=False,
                 skip_existing=False,
                 download_dir=data_dir,
                 extract_dir=extract_dir):
    """
    Download a dataset with a given date.
    """
    local_filename = f'{download_dir}/{date}.zip'
    remote_url = f'{dl_url}?f={date}'
    if skip_existing is True and path.exists(local_filename):
        print(f'Skipping {remote_url} because {local_filename} already exists')
        raise RBNDataAlreadyDownloaded
    print(f'Downloading {remote_url} to {local_filename}...')
    if unzip:
        print(f'Unzipping {local_filename} to {extract_dir} afterwards.')
        # Thanks: https://stackoverflow.com/a/39217788
        #
        # FIXME: Check download type and make sure it's a zip file
    if dryrun:
        return
    with requests.get(remote_url, stream=True) as r:
        if r.status_code == 200:
            print('Working!')
        else:
            print(f'Uh-oh: status code {r.status_code}')
            return
        with open(local_filename, 'wb') as f:
            shutil.copyfileobj(r.raw, f)
        if unzip:
            zip_ref = zipfile.ZipFile(local_filename, 'r')
            zip_ref.extractall(extract_dir)
            zip_ref.close()


@click.command('list', short_help='List data sets')
def list_sets():
    """
    List data sets available for download from ReverseBeacon.net
    """
    r = requests.get(rbn_url)
    if r.status_code == 200:
        soup = BeautifulSoup(r.text, 'lxml')
        for link in soup.find_all(href=re.compile("dl.php\?f=")):
            print(link['href'])


# Aha: click doesn't automagically add the commands to the group
# (and thus to the help output); you have to do it manually.
rbn.add_command(download)
rbn.add_command(list_sets)

# def main():
#     """Main function."""
if __name__ == '__main__':
    rbn()
