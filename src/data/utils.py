# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
from astral import Astral


def count_by_hour(df, band, cols=['total']):
    """
    Given a dataframe and a band, return a dataframe that is resampled
    and summed by hour.
    """
    temp_df = df[df.band == band].dropna()
    temp_df['total'] = 1
    temp_df = temp_df[cols]
    hourly_df = temp_df.resample('H').sum()
    return hourly_df


def add_sunspots_data(df, sunspots_df):
    """Add sunspot dataframe to another dataframe

    Note: `merge` does not support merging in place, so we have to
    return a dataframe.

    Args:
        df (Pandas dataframe): dataframe to merge sunspot data into
        sunpsots_df (Pandas dataframe): dataframe with sunspot data

    Returns:
        df (Pandas dataframe): merged dataframe
    """
    reindexed_sunspots = sunspots_df.reindex(df.index, method='pad')
    df = df.merge(reindexed_sunspots, left_index=True, right_index=True)
    return df


def add_time_columns(df):
    """Add additional time columns we're interested in.  Operates in place.

    Args:
        df (Pandas dataframe): dataframe to add time columns to
    """
    df['hour'] = df.index.hour
    df['month'] = df.index.month
    df['annual'] = df.index.strftime('%j').astype('float')
    df['annual'] /= 366.0
    # https://stackoverflow.com/questions/32278728/convert-dataframe-date-row-to-a-weekend-not-weekend-value
    df['weekend'] = ((pd.DatetimeIndex(df.index).dayofweek) // 5).astype(float)
    # Most of the time, the ARRL DX contest isn't running...
    df['arrl_dx_contest'] = 0.0
    # ...but occasionally it is:
    df.loc['2018-2-17':'2018-2-18', 'arrl_dx_contest'] = 1.0
    df.loc['2019-2-16':'2019-2-17', 'arrl_dx_contest'] = 1.0


def plot_keras_history(history):
    """
    Plot keras training history.
    """
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('Model loss')
    plt.ylabel('Loss')
    plt.xlabel('Epoch')
    plt.legend(['Train', 'Test'], loc='upper left')
    plt.show()


def group_by_hour(df):
    "Group dataframe by hour, summing by band and dx_continent"
    df = df['db'].groupby([df.index.hour, df.dx_pfx,
                           df.dx_cont]).count().fillna(0).reset_index()
    return df


def graph_by_hour(df):
    "Graph dataframe by hour, summing by band and dx_cont"
    ax = sns.relplot(x='date',
                     y='db',
                     kind='line',
                     height=8,
                     data=group_by_hour(df))
    # Set the labels for the axes appropriately.
    ax.set(xlabel='Hour (UTC)', ylabel='Stations heard')


def add_solar_elevation(df, city_name='Vancouver', city_tz='America/Vancouver'):
    """Add solar elevation column for frame.  Uses the Astral library
    (https://pypi.org/project/astral/).

    :param df: data frame; must have datetime index.

    :param city_name: name of city that Astral will understand; see
    https://astral.readthedocs.io/en/stable/index.html#cities.

    :param tz: convert UTC in the Pandas dataframe index to this time
    zone.
    """
    astral_obj = Astral()
    astral_obj.solar_depression = 'civil'
    city = astral_obj[city_name]

    # Here's what's going on here: The way we're loading from CSV
    # right now produces timezone-naive indexes.  Converting this to
    # timezone-aware indexes would probably be a Good Thing(tm), but
    # is going to end up meaning a lot of rewriting -- and may break
    # older notebooks in the process.  For now, I'm going to
    # prioritize *not* breaking old notebooks by maintaining that
    # compatibility.  It's interesting to think about how I would
    # version this code in prod; for now, I'm punting.

    # So:
    # - we assume UTC and convert the index to timezone-aware;
    # - we convert that to a series;
    # - we apply the Astral solar_elevation to that series;
    # - we convert the index of that series back to timezone-naive;
    # - and then we assign that to a column.
    df['solar_elevation'] = df.index.tz_localize(tz='UTC').to_series(keep_tz=True).apply(
    lambda x: city.solar_elevation(x)).tz_localize(None)
