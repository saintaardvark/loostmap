"""pytest framework for load_dataset.py
"""

import matplotlib
import matplotlib.pyplot as plt
from src.data.load_dataset import load_and_count_by_band

matplotlib.use('agg')


def test_load_and_count_by_band():
    """Test for load_and_count_by_band

    This is too specific for long-term use, but will help me track
    down errors for now.
    """
    data_20m = load_and_count_by_band(band='20m')
    assert data_20m.shape[1] == 7
    assert 'sunspots_count' in list(data_20m)
