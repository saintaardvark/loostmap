#!/bin/bash

# For reference:

# $ jupyter nbconvert --to python notebooks/02-hpb-arrl-dx-2019.ipynb
# [NbConvertApp] Converting notebook notebooks/02-hpb-arrl-dx-2019.ipynb to python
# [NbConvertApp] Writing 7441 bytes to notebooks/02-hpb-arrl-dx-2019.py
cat <<EOF
You want to run:

    jupyter nbconvert --to python path/to/notebook
EOF
