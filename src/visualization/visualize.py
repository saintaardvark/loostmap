# -*- coding: utf-8; eval: (fixmee-mode) -*-
import logging
from pathlib import Path

import click
import seaborn as sns
from dotenv import find_dotenv, load_dotenv
# FIXME: This only works when running from the root directory
from src.data.load_dataset import load_dataset

sns.set()
# project_dir = Path(__file__).resolve().parents[2]

# find .env automagically by walking up directories until it's found, then
# load up the .env entries as environment variables
# load_dotenv(find_dotenv())


def group_by_hour(df):
    "Group dataframe by hour, summing by band and dx_continent"
    df = df['db'].groupby([df.index.hour, df.band,
                           df.dx_cont]).count().fillna(0).reset_index()
    return df


def graph_by_hour(df, output='output.png'):
    "Graph dataframe by hour, summing by band and dx_cont"
    plot = sns.relplot(x='date',
                       y='db',
                       col='band',
                       hue='dx_cont',
                       col_wrap=2,
                       kind='line',
                       height=8,
                       data=group_by_hour(df))
    plot.savefig(output)


@click.command()
# @click.argument('input_filepath', type=click.Path(exists=True))
@click.argument('input_filepath')
def main(input_filepath):
    """Create initial graphs from dataset
    """
    logger = logging.getLogger(__name__)
    logger.info(f'loading dataset from {input_filepath}')
    data = load_dataset(input_filepath)
    ve7cc = data.loc[data.callsign == 'VE7CC']
    logger.info('making initial graphs from dataset')
    # FIXME: The name doesn't reflect anything about the date of the dataset
    graph_by_hour(ve7cc, output='ve7cc.png')
    logger.info('done')


if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    # not used in this stub but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]

    # find .env automagically by walking up directories until it's found, then
    # load up the .env entries as environment variables
    load_dotenv(find_dotenv())

    main()
