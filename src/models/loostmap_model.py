import pandas as pd
from sklearn.model_selection import cross_val_score


class LoostmapModel():
    """
    A base class for LoostMap models
    """

    def __init__(self, dataframe, model, name=""):
        "Initialize this instance"
        # Doh: "self.df = dataframe" does *not* make a copy of the frame!
        self.df = dataframe.copy()
        self.model = model
        self.name = name
        self.X = self.df.drop('total', axis=1)
        self.y = self.df.total
        self.model_coef = pd.Series()

    def add_prediction(self, col="predicted"):
        "Add model prediction to dataframe"
        self.model.fit(self.X, self.y)
        self.df[col] = self.model.predict(self.X)
        # FIXME: This is too specific; should probably subclass this model
        self.model_coef = pd.Series(self.model.coef_, index=self.X.columns)

    def print_xvalidate_accuracy(self, cv=5):
        "Do cross-validation"
        # FIXME: Do something better than this
        self.xval_score = cross_val_score(self.model, self.X, self.y, cv=cv)
        print("Accuracy for %s: %0.2f (+/- %0.2f)" %
              (self.name, self.xval_score.mean(), self.xval_score.std() * 2))
