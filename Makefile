.PHONY: clean data lint requirements sync_data_to_s3 sync_data_from_s3

#################################################################################
# GLOBALS                                                                       #
#################################################################################

PROJECT_DIR := $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))
BUCKET = [OPTIONAL] your-bucket-for-syncing-data (do not include 's3://')
PROFILE = default
PROJECT_NAME = loostmap
PYTHON_INTERPRETER = python3

ifeq (,$(shell which conda))
HAS_CONDA=False
else
HAS_CONDA=True
endif

#################################################################################
# COMMANDS                                                                      #
#################################################################################

## Install Python Dependencies
requirements: test_environment
	$(PYTHON_INTERPRETER) -m pip install -U pip setuptools wheel > /dev/null
	$(PYTHON_INTERPRETER) -m pip install -r requirements.txt > /dev/null

## Make Dataset
data: requirements first_quarter_2019 data/interim/ve7cc_eu.csv data/interim/SN_d_tot_V2.0-interim.csv data/interim/ve7cc_dx.csv

first_quarter_2019:
	# FIXME: For now, using this for interim steps.  Will need to revert.
	# $(PYTHON_INTERPRETER) src/data/make_dataset.py
	# $(PYTHON_INTERPRETER) src/data/rbn.py download --range 20190301-20190331 --unzip

second_quarter_2019:
	# $(PYTHON_INTERPRETER) src/data/make_dataset.py
	$(PYTHON_INTERPRETER) src/data/rbn.py download --range 20190401-20190628 --unzip
# Here we're looking for data where dx_cont is EU (Europe), and
# the data mode is CW (not RTTY, etc).

data_2018:
	$(PYTHON_INTERPRETER) src/data/rbn.py download --range 20180101-20181231 --unzip

# The difference between ve7cc_eu.csv and ve7cc_dx.csv is that _eu
# only contains records that have the EU as the source continent; _cx
# includes all non-North Americans continents.  _eu was used for early
# exploration, but I've shifted attention since then to all DX.

data/interim/ve7cc_eu.csv:
	head -1 /home/aardvark/dev/loostmap/data/external/20190301.csv  > $@
	grep -h VE7CC /home/aardvark/dev/loostmap/data/external/*csv | \
		awk -F',' '($$8 == "EU" && $$NF == "CW")'  >> $@
	$(PYTHON_INTERPRETER) src/data/test_loading_data.py $@

data/interim/ve7cc_dx.csv:
	head -1 /home/aardvark/dev/loostmap/data/external/20190301.csv  > $@
	grep -h VE7CC /home/aardvark/dev/loostmap/data/external/*csv | \
		awk -F',' '($$8 != "NA" && $$NF == "CW")'  >> $@
	$(PYTHON_INTERPRETER) src/data/test_loading_data.py $@

data/raw/sunspot/SN_d_tot_V2.0.csv:
	# -O uses the remote name, and -J forces the -O to get that
	# -name from the content-disposition header rather than the
	# -URL, and -L follows redirects if needed.
	curl -JLO http://www.sidc.be/silso/INFO/sndtotcsv.php

# Massaging the dates like this turns out to be a great deal easier
# than trying to do the right thing with pd.read_csv().
data/interim/SN_d_tot_V2.0-interim.csv: data/raw/sunspot/SN_d_tot_V2.0.csv
	awk -F';' '{print $$1 "-" $$2 "-" $$3 ";" $$5}' < $< > $@

## Delete all compiled Python files
clean:
	find . -type f -name "*.py[co]" -delete
	find . -type d -name "__pycache__" -delete

## Lint using flake8
lint:
	flake8 src

## Upload Data to S3
sync_data_to_s3:
ifeq (default,$(PROFILE))
	aws s3 sync data/ s3://$(BUCKET)/data/
else
	aws s3 sync data/ s3://$(BUCKET)/data/ --profile $(PROFILE)
endif

## Download Data from S3
sync_data_from_s3:
ifeq (default,$(PROFILE))
	aws s3 sync s3://$(BUCKET)/data/ data/
else
	aws s3 sync s3://$(BUCKET)/data/ data/ --profile $(PROFILE)
endif

## Set up python interpreter environment
create_environment:
ifeq (True,$(HAS_CONDA))
		@echo ">>> Detected conda, creating conda environment."
ifeq (3,$(findstring 3,$(PYTHON_INTERPRETER)))
	conda create --name $(PROJECT_NAME) python=3
else
	conda create --name $(PROJECT_NAME) python=2.7
endif
		@echo ">>> New conda env created. Activate with:\nsource activate $(PROJECT_NAME)"
else
	$(PYTHON_INTERPRETER) -m pip install -q virtualenv virtualenvwrapper
	@echo ">>> Installing virtualenvwrapper if not already intalled.\nMake sure the following lines are in shell startup file\n\
	export WORKON_HOME=$$HOME/.virtualenvs\nexport PROJECT_HOME=$$HOME/Devel\nsource /usr/local/bin/virtualenvwrapper.sh\n"
	@bash -c "source `which virtualenvwrapper.sh`;mkvirtualenv $(PROJECT_NAME) --python=$(PYTHON_INTERPRETER)"
	@echo ">>> New virtualenv created. Activate with:\nworkon $(PROJECT_NAME)"
endif

## Test python environment is setup correctly
test_environment:
	$(PYTHON_INTERPRETER) test_environment.py

test:
	pytest -v --cov=src

notebook:
	@bash -c "source venv/bin/activate && jupyter notebook --no-browser"

#################################################################################
# PROJECT RULES                                                                 #
#################################################################################



#################################################################################
# Self Documenting Commands                                                     #
#################################################################################

.DEFAULT_GOAL := help

# Inspired by <http://marmelab.com/blog/2016/02/29/auto-documented-makefile.html>
# sed script explained:
# /^##/:
# 	* save line in hold space
# 	* purge line
# 	* Loop:
# 		* append newline + line to hold space
# 		* go to next line
# 		* if line starts with doc comment, strip comment character off and loop
# 	* remove target prerequisites
# 	* append hold space (+ newline) to line
# 	* replace newline plus comments by `---`
# 	* print line
# Separate expressions are necessary because labels cannot be delimited by
# semicolon; see <http://stackoverflow.com/a/11799865/1968>
.PHONY: help
help:
	@echo "$$(tput bold)Available rules:$$(tput sgr0)"
	@echo
	@sed -n -e "/^## / { \
		h; \
		s/.*//; \
		:doc" \
		-e "H; \
		n; \
		s/^## //; \
		t doc" \
		-e "s/:.*//; \
		G; \
		s/\\n## /---/; \
		s/\\n/ /g; \
		p; \
	}" ${MAKEFILE_LIST} \
	| LC_ALL='C' sort --ignore-case \
	| awk -F '---' \
		-v ncol=$$(tput cols) \
		-v indent=19 \
		-v col_on="$$(tput setaf 6)" \
		-v col_off="$$(tput sgr0)" \
	'{ \
		printf "%s%*s%s ", col_on, -indent, $$1, col_off; \
		n = split($$2, words, " "); \
		line_length = ncol - indent; \
		for (i = 1; i <= n; i++) { \
			line_length -= length(words[i]) + 1; \
			if (line_length <= 0) { \
				line_length = ncol - indent - length(words[i]) - 1; \
				printf "\n%*s ", -indent, " "; \
			} \
			printf "%s ", words[i]; \
		} \
		printf "\n"; \
	}' \
	| more $(shell test $(shell uname) = Darwin && echo '--no-init --raw-control-chars')
