Getting started
===============

This is where you describe how to get set up on a clean install, including the
commands necessary to get the raw data (using the `sync_data_from_s3` command,
for example), and then how to make the cleaned, final data sets.

Sunspot data
^^^^^^^^^^^^

Sunspot data comes from the World Data Center SILSO, Royal Observatory
of Belgium, Brussels (http://www.sidc.be/silso/datafiles), and is
licensed under Creative Commons BY-NC 4.0
(https://creativecommons.org/licenses/by-nc/4.0/).

The format is described at http://www.sidc.be/silso/newdataset:

Contents: year, month, day, decimal year, SNvalue , SNerror, Nb observations
